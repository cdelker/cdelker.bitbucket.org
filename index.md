<link rel="stylesheet" href="css/splendor.css">

## Collin's Software

- [SchemDraw](https://schemdraw.readthedocs.io) Electrical schematic and flowchart drawing in Python
- [Ziamath](https://ziamath.readthedocs.io) Draw MathML and Latex expressions to SVG in Python
- [Ziafont](https://ziafont.readthedocs.io) Read OpenType/TrueType fonts and draw glyphs to SVG
- [Ziaplot](https://ziaplot.readthedocs.io) Lightweight plotting library for Python
- [TacoTUI](https://tacotui.readthedocs.io) Terminal User Interface and coloring for Python
- [Segmented Bowl Designer](segbowl/index.html) for woodturning, online


- [Pyodide Minimal Examples](https://cdelker.github.io/pyodide-test/) Some minimal working examples for using Pyodide, running Python code browser-side.

### Repository

[Bitbucket Repository](https://bitbucket.org/cdelker)
